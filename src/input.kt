import java.util.*

object tictactoe {
    var kb: Scanner? = null
    var board =
            Array(3) { arrayOfNulls<String>(3) }
    var turn: String? = null
    var winner: String? = null
    var num = 0
    var inputR = 0
    var inputC = 0

    @JvmStatic
    fun main(args: Array<String>) {
        kb = Scanner(System.`in`)
        startBoard()
        check()
        checkDraw(winner)
    }

    fun setInt() {
        inputR = 0
        inputC = 0
    }

    fun startBoard() {
        turn = "X"
        println("Welcome to Tic Tac Toe.\n")
        for (i in 0..2) {
            for (j in 0..2) {
                board[i][j] = "-"
            }
        }
        printBoard()
        print(turn + "turn." + "Please input row and column here: ")
    }

    fun printBoard() {
        println("     " + "1  " + " 2  " + " 3  ")
        println()
        println(
                "1    " + board[0][0] + "   " + board[0][1] + "   " + board[0][2]
        )
        println()
        println(
                "2    " + board[1][0] + "   " + board[1][1] + "   " + board[1][2]
        )
        println()
        println(
                "3    " + board[2][0] + "   " + board[2][1] + "   " + board[2][2]
        )
        println()
    }

    fun check() {
        while (winner == null) {
            try {
                inputR = kb!!.nextInt()
                inputC = kb!!.nextInt()
                if (inputR == 1 && inputC == 1 && board[0][0] == "-") {
                    board[0][0] = turn
                    turn = if (turn == "X") {
                        "O"
                    } else "X"
                    printBoard()
                    winner = checkWinner()
                    num++
                    setInt()
                } else if (inputR == 1 && inputC == 2 && board[0][1] == "-") {
                    board[0][1] = turn
                    turn = if (turn == "X") {
                        "O"
                    } else "X"
                    printBoard()
                    winner = checkWinner()
                    num++
                    setInt()
                } else if (inputR == 1 && inputC == 3 && board[0][2] == "-") {
                    board[0][2] = turn
                    turn = if (turn == "X") {
                        "O"
                    } else "X"
                    printBoard()
                    winner = checkWinner()
                    num++
                    setInt()
                } else if (inputR == 2 && inputC == 1 && board[1][0] == "-") {
                    board[1][0] = turn
                    turn = if (turn == "X") {
                        "O"
                    } else "X"
                    printBoard()
                    winner = checkWinner()
                    num++
                    setInt()
                } else if (inputR == 2 && inputC == 2 && board[1][1] == "-") {
                    board[1][1] = turn
                    turn = if (turn == "X") {
                        "O"
                    } else "X"
                    printBoard()
                    winner = checkWinner()
                    num++
                    setInt()
                } else if (inputR == 2 && inputC == 3 && board[1][2] == "-") {
                    board[1][2] = turn
                    turn = if (turn == "X") {
                        "O"
                    } else "X"
                    printBoard()
                    winner = checkWinner()
                    num++
                    setInt()
                } else if (inputR == 3 && inputC == 1 && board[2][0] == "-") {
                    board[2][0] = turn
                    turn = if (turn == "X") {
                        "O"
                    } else "X"
                    printBoard()
                    winner = checkWinner()
                    num++
                    setInt()
                } else if (inputR == 3 && inputC == 2 && board[2][1] == "-") {
                    board[2][1] = turn
                    turn = if (turn == "X") {
                        "O"
                    } else "X"
                    printBoard()
                    winner = checkWinner()
                    num++
                    setInt()
                } else if (inputR == 3 && inputC == 3 && board[2][2] == "-") {
                    board[2][2] = turn
                    turn = if (turn == "X") {
                        "O"
                    } else "X"
                    printBoard()
                    winner = checkWinner()
                    num++
                    setInt()
                } else {
                    print("re-enter slot number: ")
                    setInt()
                    continue
                }
            } catch (e: InputMismatchException) {
                print("Input mismatch!!")
                break
            }
        }
    }

    fun checkWinner(): String? {
        for (a in 0..7) {
            var line: String? = null
            when (a) {
                0 -> line =
                        board[0][0] + board[0][1] + board[0][2]
                1 -> line =
                        board[1][0] + board[1][1] + board[1][2]
                2 -> line =
                        board[2][0] + board[2][1] + board[2][2]
                3 -> line =
                        board[0][0] + board[1][0] + board[2][0]
                4 -> line =
                        board[0][1] + board[1][1] + board[2][1]
                5 -> line =
                        board[0][2] + board[1][2] + board[2][2]
                6 -> line =
                        board[0][0] + board[1][1] + board[2][2]
                7 -> line =
                        board[2][0] + board[1][1] + board[0][2]
            }
            if (line == "XXX") {
                return "X"
            } else if (line == "OOO") {
                return "O"
            }
        }
        for (a in 0..8) {
            if (num >= 8) return "draw"
        }
        print("$turn[R,C]: ")
        return null
    }

    fun checkDraw(winner: String?) {
        if (winner.equals("draw", ignoreCase = true)) {
            println("It's a draw! Thanks for playing.")
            setInt()
        } else if (winner == null) {
            println("Please restart game.")
            setInt()
        }
        else {
            println("Congratulations! $winner's have won! Thanks for playing.")
            setInt()
        }
    }
}
